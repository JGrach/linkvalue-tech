# Linkvalue Test

## run

```bash
npm i && npm run dev
```

or

```bash
npm i && npm run build && npm run start
```

## relevant

-   [component page](https://gitlab.com/JGrach/linkvalue-tech/-/blob/master/app/pages/index.tsx)
-   [csv formatting](https://gitlab.com/JGrach/linkvalue-tech/-/blob/master/app/lib/vaccinationCsv.ts)
