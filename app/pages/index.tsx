import { formatVaccinationCsv, getVaccinationCsv, VaccinationData } from 'app/lib/vaccinationCsv';
import React from 'react';

interface LayoutProps {
    vaccinatedByCountry: {
        header: VaccinationData;
        body: VaccinationData[];
    };
}

const IndexPage = ({ vaccinatedByCountry }: LayoutProps) => {
    const vaccinatedHeader = vaccinatedByCountry.header;

    // Could be sorted in formatVaccinationCsv but "sort" is only for display: device responsability
    const vaccinatedBody = vaccinatedByCountry.body.sort((a, b) => {
        return Number(b[vaccinatedHeader.vaccinations]) - Number(a[vaccinatedHeader.vaccinations]);
    });

    return (
        <>
            <table>
                <thead>
                    <th>{vaccinatedHeader.country}</th>
                    <th>{vaccinatedHeader.date}</th>
                    <th>{vaccinatedHeader.vaccinations}</th>
                </thead>
                <tbody>
                    {vaccinatedBody.map(data => (
                        <tr key={data[vaccinatedHeader.country]}>
                            <td>{data[vaccinatedHeader.country]}</td>
                            <td>{data[vaccinatedHeader.date]}</td>
                            <td>{data[vaccinatedHeader.vaccinations]}</td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </>
    );
};

export async function getStaticProps() {
    const csv = await getVaccinationCsv();

    return {
        props: {
            vaccinatedByCountry: formatVaccinationCsv(csv),
        },
    };
}

export default IndexPage;
