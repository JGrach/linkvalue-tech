import { createGlobalStyle } from 'styled-components';

export const GlobalStyle = createGlobalStyle`
  div#__next, html, body {
    margin: 0;
    height: 100%;
    width: 100%;
  };
  table {
    width: 100%;
    border-collapse : collapse;
  }
  table td, th {
    border: 1px solid black
    text-align: center
    padding: 20px
  }
`;

export default GlobalStyle;
