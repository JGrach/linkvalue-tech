export async function getVaccinationCsv() {
    const res = await fetch('http://cchampou.dyndns.org:7000/data.csv');
    return res.text();
}

// I've choosen to keep your data exactly the same
// So keys are not empirically defined but come from CSV:
// Pair key/value always true but low typing
export interface VaccinationData {
    [keys: string]: string;
}

// warn: complexity -> update comments or simplify
export function formatVaccinationCsv(vaccinationCsv: string) {
    const countryIndex = 0;
    const dateIndex = 2;
    const vaccintationIndex = 10;

    const lines = vaccinationCsv.split('\n');
    const headerLine = lines.shift().split(',');
    const headers = {
        country: headerLine[countryIndex],
        date: headerLine[dateIndex],
        vaccinations: headerLine[vaccintationIndex],
    };

    const vaccinationDatas = lines.reduce<VaccinationData[]>((acc, line) => {
        // string line to array element:
        const datas = line.split(',');

        // extract relevant data:
        const country = datas[countryIndex];
        const date = datas[dateIndex];
        const vaccinations = datas[vaccintationIndex];

        // Don't keep if nothing change
        const indexExisting = acc.findIndex(vaccinationData => vaccinationData[headers.country] === country);
        const shouldUpdate = indexExisting === -1 || new Date(acc[indexExisting][headers.date]) < new Date(date);
        if (!date || !vaccinations || !shouldUpdate) return acc;

        // will be updated
        const countryData = {
            [headers.country]: country,
            [headers.date]: date,
            [headers.vaccinations]: vaccinations,
        };

        // Add at last if doesn't exist
        if (indexExisting === -1) return [...acc, countryData];

        // Change the outdated if exist
        return [...acc.slice(0, indexExisting), countryData, ...acc.slice(indexExisting + 1)];
    }, []);

    return {
        header: headers,
        body: vaccinationDatas,
    };
}
