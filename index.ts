import next from 'next';
import path from 'path';
import server from './server';

const rootDir = path.resolve(__dirname);
const port = parseInt(process.env.PORT, 10) || 3000;
const dev = process.env.NODE_ENV !== 'production';
const app = next({ dev, dir: `${rootDir}/app` });
const handle = app.getRequestHandler();

app.prepare()
    .then(server(port, handle))
    .catch(err => console.error(err));
